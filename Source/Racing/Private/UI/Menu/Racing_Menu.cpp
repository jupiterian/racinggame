// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Racing.h"
#include "UI/Menu/Racing_Menu.h"
#include "UI/Menu/VehicleHUD_Menu.h"

ARacing_Menu::ARacing_Menu(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	HUDClass = AVehicleHUD_Menu::StaticClass();
}

void ARacing_Menu::RestartPlayer(AController* NewPlayer)
{
	// don't restart
}
