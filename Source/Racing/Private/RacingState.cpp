// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Racing.h"
#include "RacingState.h"

ARacingState::ARacingState(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	NumRacers = 0;
	TotalTime = 0;
	bTimerPaused = false;
	bIsRaceActive = false;
	// need to tick when paused to check king state.
	PrimaryActorTick.bCanEverTick = true;
	SetTickableWhenPaused(true);
}

void ARacingState::GetLifetimeReplicatedProps( TArray< FLifetimeProperty > & OutLifetimeProps ) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );

	DOREPLIFETIME( ARacingState, NumRacers );
	DOREPLIFETIME( ARacingState, TotalTime );
	DOREPLIFETIME( ARacingState, bTimerPaused );
	DOREPLIFETIME( ARacingState, bIsRaceActive );
	
}

float ARacingState::GetTotalTime()
{
	return TotalTime;
}

bool ARacingState::IsRaceActive() const
{
	return bIsRaceActive;
}