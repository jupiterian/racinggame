// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Racing : ModuleRules
{
	public Racing(ReadOnlyTargetRules Target) : base(Target)
	{
		PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"CoreUObject",
				"Engine",
				"Landscape",
                "PhysXVehicles"
			}
		);

		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"InputCore",
				"Slate",
				"SlateCore",
				"RacingLoadingScreen",
			}
		);

		PrivateIncludePaths.AddRange(
			new string[] {
				"Racing/Private/UI/Widgets",
				"Racing/Private/UI/Style",
			}
		);
	}
}
